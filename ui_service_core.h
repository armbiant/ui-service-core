/**
 * @file
 * @author Martin Stejskal
 * @brief UI service module
 *
 * Allow to configure device through service interface (UART for example)
 */
#ifndef __UI_SERVICE_H__
#define __UI_SERVICE_H__
// ===============================| Includes |================================
// Standard libraries
#include <stdbool.h>
#include <stdint.h>
// ================================| Defines |================================
/**
 * @brief Some reasonable period within UI task is called in ms
 *
 * Recommended value is 50
 */
#define UI_SERVICE_TASK_RECOMMENDED_PERIOD_MS (50)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// =======================| Structures, enumerations |========================
/**
 * @brief List of error codes for UI service module
 */
typedef enum {
  USC_OK = 0,
  USC_ERROR = 1,  ///@brief Generic error
  USC_ERROR_TIMEOUT,
  USC_ERROR_EMPTY_POINTER,
  USC_ERROR_INVALID_PARAMETER,
  USC_ERROR_NOT_CONFIGURED,
  USC_ERROR_NO_MEMORY,
  USC_ERROR_NOT_FOUND,
} te_usc_error_code;

typedef enum {
  KEY_NONE = 0,  //!< No special key detected
  KEY_ENTER,     //!< Enter key detected
  KEY_ESC,       //!< Escape key detected
  KEY_MAX,       //!< Signalize last entry in enumeration
} te_usc_special_key;

/**
 * @brief This structure is passed to every called command
 */
typedef struct {
  /**
   * @brief Pointer to input buffer
   *
   * This include command string and all arguments
   */
  char *pac_input_string;

  /**
   * @brief Pointer to argument
   *
   * Basically same as pac_input_string, but without command
   */
  char *pac_args;

  /**
   * @brief Convert argument to string if match with argument type
   *
   * If argument type is in decadic or hexadecimal format, it will be
   * automatically converted from string
   *
   */
  union {
    // In case of argument type "INT", signed integer will be used
    int32_t i32_value;

    // In case of argument type "HEX", unsigned integer is used
    uint32_t u32_value;

    // And if someone wants, it can be read as Byte array
    uint8_t au8[sizeof(uint32_t)];
  } u_value;

} ts_usc_cb_args;

/**
 * @brief Define callback for user commands
 *
 * @param ps_args Structure of arguments. For details refer
 *                to @ref ts_usc_cb_args
 */
typedef void (*tf_usc_cmd_callback)(ts_usc_cb_args *ps_args);

/**
 * @brief Structure for describing user command
 *
 * When core request list of all command, array of those  structures is
 * expected.
 */
typedef struct {
  /**
   * @brief Command as string
   *
   * @note Have to be setup. If not set, it will cause assert error
   */
  char *pac_command;

  /**
   * @brief Help string
   *
   * @note Have to be setup. If not set, it will cause assert error
   */
  char *pac_help;

  /**
   * @brief Pointer to function which will be called if command is identified
   *
   * As input parameter is given string from service interface, so called
   * function can process arguments.
   *
   * @note Have to be setup. If not set, it will cause assert error
   */
  tf_usc_cmd_callback pf_cmd_callback;

  ///@brief Does command require any argument?
  bool b_require_argument;

  // Following is relevant only if argument is required
  struct {
    /**
     * @brief Argument type
     */
    enum {
      UI_SRVCE_CMD_ARG_TYPE_INT,  //!< Integer. Signed 32 bit value
      UI_SRVCE_CMD_ARG_TYPE_HEX,  //!< Hex. Unsigned 32 bit value
      UI_SRVCE_CMD_ARG_TYPE_STR   //!< String or other. Not specified.
    } e_arg_type;

    /**
     * @brief Limits for integer
     *
     * Valid only if argument type is integer or hex. If both are zeros, it
     * means "no limit" -> no checking. This might be applied for some cases
     * when hex type is used.
     */
    int32_t i32_min, i32_max;

    /**
     * @brief If argument is required, it is good idea to provide example
     *
     * This example will be used as part of automatically generated
     * documentation. If not defined and argument is required, generic string
     * will be printed to help
     */
    char *pac_argument_example;

  } s_arg_cfg;

} ts_usc_cmd_cfg;

/**
 * @brief Callbacks used in configuration argument
 * @{
 */
// Interface related
typedef te_usc_error_code (*tf_usc_itf_read_cb)(uint8_t *pu8_data,
                                                uint16_t u16_buffer_length,
                                                uint16_t u16_timeout_ms,
                                                uint16_t *pu_num_of_read_bytes);

typedef te_usc_error_code (*tf_usc_itf_write_cb)(const uint8_t *pu8_data,
                                                 uint16_t u16_data_length,
                                                 uint16_t u16_timeout_ms);

// Basic core functions linked to UI service user part
typedef const char *(*tf_usc_get_password_cb)(void);
typedef const ts_usc_cmd_cfg *(*tf_usc_get_list_of_cmds_cb)(void);
typedef uint16_t (*tf_usc_get_num_of_cmds_cb)(void);

// Callbacks useful for application layers (to inform about switching into
// or from service mode
typedef void (*tf_usc_enter_service_mode_cb)(void);
typedef bool (*tf_usc_enter_service_mode_is_ready_cb)(void);
typedef void (*tf_usc_leave_service_mode_cb)(void);
typedef bool (*tf_usc_leave_service_mode_is_ready_cb)(void);

// General functions, which might sounds redundant, but actually they are not.
// For example delay might be dummy blocking function, or it can be RTOS's
// function which can actually actively use time for other processing.
typedef void (*tf_usc_delay_cb)(const uint16_t u16_delay_ms);
/**
 * @}
 */

typedef struct {
  /**
   * @brief Function that reads raw data from interface
   */
  tf_usc_itf_read_cb pf_read_itf_cb;

  /**
   * @brief Function that writes raw data to interface
   */
  tf_usc_itf_write_cb pf_write_itf_cb;

  /**
   * @brief Returns password necessary for login
   *
   * @note Optional. If empty (set to NULL), then no password is required
   */
  tf_usc_get_password_cb pf_get_password_cb;

  /**
   * @brief Function that returns list of all available commands
   */
  tf_usc_get_list_of_cmds_cb pf_get_list_of_cmds_cb;

  /**
   * @brief Function that tells how many commands are available
   *
   * This is complement function to the @ref pf_get_list_of_cmds_cb
   */
  tf_usc_get_num_of_cmds_cb pf_get_num_of_cmds_cb;

  /**
   * @brief Called before entering service mode to notify upper layer
   *
   * @note Optional. Can be empty. It can help application layer to postpone
   *       or plan to postpone some other tasks for example.
   * @note Active only if password is required. Otherwise it is assumed that
   *       service is available all the time
   */
  tf_usc_enter_service_mode_cb pf_enter_service_mode_cb;

  /**
   * @brief Before entering actually service mode, check if entering is allowed
   *
   * It can only help upper layer to finish routines before UI core move to
   * service mode (which might control some system parts). Core will wait until
   * "true" is returned.
   *
   * @note Optional. Can be empty. If empty, then assume that core can enter
   *       service mode immediately.
   * @note Active only if password is required. Otherwise it is assumed that
   *       service is available all the time
   */
  tf_usc_enter_service_mode_is_ready_cb pf_enter_service_mode_is_ready_cb;

  /**
   * @brief Called when leaving service mode
   *
   * @note Optional as "enter" function. Meaning is basically same.
   *
   * @note Active only if password is required. Otherwise it is assumed that
   *       service is available all the time
   */
  tf_usc_leave_service_mode_cb pf_leave_service_mode_cb;

  /**
   * @brief If set, it waits for "approval" from upper layer
   *
   * This help to notify upper layer that UI core leaving service mode
   * (logout). Waits until function return "true" (means: ready).
   *
   * @note Optional. If empty, then core assume it can leave service mode now
   *
   * @note Active only if password is required. Otherwise it is assumed that
   *       service is available all the time
   */
  tf_usc_leave_service_mode_is_ready_cb pf_leave_service_mode_is_ready_cb;

  /**
   * @brief Custom delay function
   *
   * When RTOS is used, you might want to take advantage of it. Or you might
   * have your own system for delays. Or you might want to put system into
   * sleep mode.
   */
  tf_usc_delay_cb pf_delay_ms_cb;
} ts_usc_config_args;
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Configure core and report if everything is fine or not
 * @param[in] ps_args Pointer to arguments. Copy is made, so after executing
 *            this function it is not necessary to keep arguments in RAM
 * @return USC_OK if no problem found
 */
te_usc_error_code usc_configure(ts_usc_config_args *ps_args);

/**
 * @brief One "round" of execution of UI service task
 *
 * Call this task periodically in order to process input from user smoothly.
 * Recommended interval is about 35 ms.
 *
 * @warning Function expect that communication interface (typically UART) is
 *          already initialized and ready. If not, it might cause unexpected
 *          failures.
 * @warning It is up to upper layer to also handle interface deinitialization
 *          if required. This layer can not know when "is right time to
 *          deinitialize" communication interface
 *
 * @return USC_OK if no problem found
 */
te_usc_error_code usc_task_exec(void);

/**
 * @brief Reset task state machine
 */
void usc_task_reset(void);
// ========================| Middle level functions |=========================

/**
 * @brief Wait until user press "enter" or "escape" button
 *
 * Blocking function.
 *
 * @param[out] pac_user_input_buffer Pointer to buffer. User input is written
 * here
 * @return Special key as enumeration. If no key is found, the KEY_NONE value
 *         is returned.
 */
te_usc_special_key usc_wait_for_enter_esc(char *pac_user_input_buffer);

/**
 * @brief Only check if user pressed "enter" or "escape" button
 *
 * Not fully blocking. Just check and get out
 *
 * @param[out] pac_user_input_buffer Pointer to buffer. User input is written
 *                                   here
 * @return Special key as enumeration. If no key is found, the KEY_NONE value
 *         is returned.
 */
te_usc_special_key usc_check_for_enter_esc(char *pac_user_input_buffer);
// ==========================| Low level functions |==========================
/**
 * @brief Print string to service interface
 * @param[in] pac_string Pointer to string. Have to be terminated by NULL
 *            character
 */
void usc_print_str(const char *pac_string);

/**
 * @brief Printf-like function
 *
 * Little bit more heavy function compare than to uiServicePrintStr
 *
 * @param[in] fmt Same as printf()
 */
void usc_printf(const char *fmt, ...);

/**
 * @brief Print OK or fail and reason
 *
 * Simple function which easily report error code status
 *
 * @param e_err_code Input error code
 */
void usc_print_ok_fail(te_usc_error_code e_err_code);

/**
 * @brief Convert error code into string
 *
 * @param e_error_code Error code as enumeration value
 * @return Pointer to string, which describe error code value
 */
const char *usc_error_code_to_string(te_usc_error_code e_error_code);
// ===========================| Predefined texts |============================
/**
 * @brief Some common strings which might be reused by other modules
 *
 * Instead of copy/pasting some commonly used strings, some of them can be
 * re-used. This also simplify changing look and language.
 *
 * @{
 */
const char *usc_txt_ok(void);
const char *usc_txt_fail(void);
const char *usc_txt_ivalid_arg(void);
const char *usc_txt_can_not_convert_to_int(void);
const char *usc_txt_out_of_range(void);
/**
 * @}
 */
#endif  // __UI_SERVICE_H__
