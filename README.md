# About
 * This is user interface between ESP32 device and PC
 * Communication is done via additional UART interface, so it will not
   interfere with ESP32 log system
 * This is only core. It is required to create *user UI*, which define all
   remaining functionality, which can be project dependent

# Configuration
 * Call `usc_configure()` and follow documentation for argument structure. Some
   setting is optional, some not.

# Example application
 * Go to `example` folder and build project. You can take advantages of
   scripts under `scripts` folder
 * Flash ESP
 * Connect UART to following pins. Direction is from ESP32 side:
   * RX: GPIO 16
   * TX: GPIO 17
   * Do not forget to connect gounds!
   * Serial settings: `115200-8-N-1`

 * When you're connected to UART mentioned above, reset ESP. On service UART
   you should see something like this:

```
== Login ==
password:
```

 * Type default password `nopenope` and press *enter*

```
== Login ==
password: nopenope
[OK]


==<| Help |>==
 Here is list of available commands:
   get name - Return previously set name
   set name - Set new name
   say hello - Say hello to defined name. Also here is example of wrapping
               relatively long description of argument. So remember that you do
               not need to worry about it.
   get number - Return previously stored number of dig up some default
   set number 5678 - Set new number into system
   default - restore default values (factory settings)
   reboot - reboot device immediately
   exit - logout

$
```

 * Try to type `say hello` or other commands :)
