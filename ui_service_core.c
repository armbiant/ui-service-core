/**
 * @file
 * @author Martin Stejskal
 * @brief UI service module
 *
 * Allow to configure device through service interface (UART for example)
 */
// ===============================| Includes |================================
#include "ui_service_core.h"

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "ui_service_core_cfg.h"  // Default settings for reference
#include "ui_service_user.h"
#include "ui_service_utils.h"

// ================================| Defines |================================
#define _LOADING_PERIOD_MS (200)

/**
 * @brief Shorten form for comparing commands which does not requires parameter
 */
#define _CMD_NO_PARAM(cmd_str) (strcmp(cmd_str, pac_user_input_buffer) == 0)

/**
 * @brief Shorted form for comparing commands which requires parameter
 */
#define _CMD_WITH_PARAM(cmd_str) \
  (memcmp(cmd_str, pac_user_input_buffer, strlen(cmd_str)) == 0)

/**
 * @brief Maximum allowed delay for delay function
 *
 * Using 16bit unsigned values, so .... yeah 0xFFFF is the maximum
 */
#define _MAX_DELAY_MS (0xFFFF)

/**
 * @brief Core commands
 *
 * @{
 */
#define CMD_EXIT "exit"
/**
 * @}
 */
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
#if (UI_SERVICE_RX_BUFFER_SIZE == 0)
#error "The UI_SERVICE_RX_BUFFER_SIZE can not be zero!"
#endif

#if (UI_SERVICE_HELP_MAX_CHAR_PER_LINE == 0)
#error "The UI_SERVICE_HELP_MAX_CHAR_PER_LINE can not be zero"
#endif

#if (UI_SERVICE_PRINTF_BUFFER == 0)
#error "The UI_SERVICE_PRINTF_BUFFER can not be zero"
#endif
// ===========================| Structures, enums |===========================
typedef struct {
  char *pac_help_space;
  char *pac_default_arg;
  char *pac_cmd_and_help_separator;
} ts_usc_help_text;

typedef enum {
  UI_SRVCE_WAIT_FOR_LOGIN,  //!< Waiting for login state
  UI_SRVCE_WAIT_FOR_CMD,    //!< Waiting for command state
  UI_SRVCE_PROCESS_CMD,     //!< Processing command state
} te_usc_state;

typedef struct {
  // Configuration structure
  ts_usc_config_args s_cfg;

  // Actual state in task state machine
  te_usc_state e_state_actual;

  // Keep track about previous state. Simulate jump from "CMD" (exit command)
  te_usc_state e_state_previous;

  // User input buffer for complete user input
  char ac_user_input_buffer[UI_SERVICE_RX_BUFFER_SIZE];

  // Write index for user input buffer
  uint16_t u16_user_input_buffer_write_idx;
} ts_usc_runtime;
// ===========================| Global variables |============================
static const ts_usc_help_text ms_help_txt = {
    .pac_help_space = "   ",
    .pac_default_arg = "arg",
    .pac_cmd_and_help_separator = " - ",
};

static ts_usc_runtime ms_runtime = {
    .e_state_actual = UI_SRVCE_WAIT_FOR_LOGIN,
    .e_state_previous = UI_SRVCE_PROCESS_CMD,
};

/**
 * @brief Header of help
 */
static const char *pac_help_header =
    "\n\n==<| Help |>==\n"
    " Here is list of available commands:\n";

/**
 * @brief Tail of help
 */
static const char *pac_help_if_logged = "   " CMD_EXIT " - logout\n";

/**
 * @brief Basic messages
 *
 * Can be shared over UI service module
 *
 * @{
 */
static const char *pac_login = "\n== Login ==";
static const char *pac_ok = "[OK]";
static const char *pac_fail = "[FAIL]";
static const char *pac_invalid_password = "Incorrect password\n";
static const char *pac_unknown_cmd = "Unknown command :/\n";
static const char *pac_press_enter_when_ready = "Press enter when ready";
static const char *pac_log_off = "Bye bye. See you next time ;)\n";
static const char *pac_invalid_argument = "Invalid argument";
static const char *pac_can_not_convert = "Can not convert value to integer\n";
static const char *pac_out_of_range = "Value is out of range\n";
/**
 * @}
 */
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
/**
 * @brief Waiting for user to input correct login password
 *
 * @param pe_state_prev Pointer to previous state
 * @param pe_state_actual Pointer to actual state
 * @param pac_user_input_buffer Pointer to user input buffer
 * @param pu16_user_input_buffer_write_idx Current buffer index (up to where
 *                                         are user data with reference to the
 *                                         input buffer)
 * @param ps_cfg Pointer to configuration structure
 */
static void _wait_for_login(te_usc_state *pe_state_prev,
                            te_usc_state *pe_state_actual,
                            char *pac_user_input_buffer,
                            uint16_t *pu16_user_input_buffer_write_idx,
                            const ts_usc_config_args *ps_cfg);

/**
 * @brief Waiting for input command
 *
 * Already logged in
 *
 * @param pe_state_previous Pointer to previous state
 * @param pe_state_actual Pointer to actual state
 * @param pac_user_input_buffer Pointer to user input buffer
 * @param pu16_user_input_buffer_write_idx Current buffer index (up to where
 *                                         are user data with reference to the
 *                                         input buffer)
 * @param ps_cfg Pointer to configuration structure
 */
static void _wait_for_cmd(te_usc_state *pe_state_previous,
                          te_usc_state *pe_state_actual,
                          char *pac_user_input_buffer,
                          uint16_t *pu16_user_input_buffer_write_idx,
                          const ts_usc_config_args *ps_cfg);

/**
 * @brief Process input command
 *
 * Already logged in and no longer waiting for input command
 *
 * @param pe_state_previous Pointer to previous state
 * @param pe_state_actual Pointer to actual state
 * @param pac_user_input_buffer Pointer to user input buffer
 * @param pu16_user_input_buffer_write_idx Current buffer index (up to where are
 * user data with reference to the input buffer)
 * @param ps_cfg Pointer to configuration structure
 */
static void _process_cmd(te_usc_state *pe_state_previous,
                         te_usc_state *pe_state_actual,
                         char *pac_user_input_buffer,
                         uint16_t *pu16_user_input_buffer_write_idx,
                         const ts_usc_config_args *ps_cfg);

/**
 * @brief Prints help for user
 * @return Error code - zero if no error, non-zero otherwise.
 */
static te_usc_error_code _print_help(void);
// ==============| Internal function prototypes: middle level |===============
/**
 * @brief Check if user input is recognized as command or not
 *
 * @param pac_user_input_buffer Pointer to user input buffer
 * @param pu16_user_input_buffer_write_idx Current buffer index (up to where
 *                                         are user data with reference to the
 *                                         input buffer)
 * @return Returns true if recognized as command. False otherwise.
 */
static bool _was_user_cmd_recognized_and_processed(
    char *pac_user_input_buffer, uint16_t *pu16_user_input_buffer_write_idx);

/**
 * @brief Wait until user finish some action by pressing special key
 * @param pac_user_input_buffer Input buffer
 * @param pu16_user_input_buffer_write_idx Write index for that buffer
 * @param pac_initial_string Print extra string. Useful for questions
 * @param b_force_reset Force reset internal logic. Use it wisely
 * @return Return which special key was used to terminate current input
 */
static te_usc_special_key _wait_for_user(
    char *pac_user_input_buffer, uint16_t *pu16_user_input_buffer_write_idx,
    const char *pac_initial_string, const bool b_force_reset);

/**
 * @brief Print string and wrap it if needed
 * @param pac_help String to be printed
 * @param u16_position_of_help_content Offset from left. Define how many
 *                                     characters should be reserved from left
 *                                     side (kind of number of spaces on left
 *                                     side)
 */
static void _print_help_wrap_lines(char *pac_help,
                                   uint16_t u16_position_of_help_content);
// ================| Internal function prototypes: low level |================
/**
 * @brief Append data from source buffer to destination buffer
 *
 * @param pac_src Pointer to source buffer
 * @param u16_src_size Number of Bytes which will be copied to destination
 *                     buffer
 * @param pac_dst Pointer to destination buffer
 * @param pu16_dst_write_idx Actual write index for destination buffer. From
 *                           this index will be written data from source
 *                           buffer.
 * @param u16_dst_buffer_size Total size of destination buffer. This will help
 *                            to detect potential buffer overflow.
 * @return Non zero value if there is problem.
 */
static te_usc_error_code _append_buffer(const char *pac_src,
                                        const uint16_t u16_src_size,
                                        char *pac_dst,
                                        uint16_t *pu16_dst_write_idx,
                                        const uint16_t u16_dst_buffer_size);

/**
 * @brief Find special character in input buffer
 * @param e_key Key character
 * @param pac_buff Pointer to buffer with characters
 * @param u16_max_idx Write index - up to this index will be searching.
 * @param pu16_key_idx Index where key was found
 * @return True if new line found, false otherwise
 */
static bool _find_special_key(const te_usc_special_key e_key,
                              const char *pac_buff, const uint16_t u16_max_idx,
                              uint16_t *pu16_key_idx);

/**
 * @brief Check if arguments are valid
 *
 * If argument is string, do not check anything. This is up to user side. But
 * in case of HEX or integer, simple check can be done
 *
 * @param ps_cb_args Pointer to structure which will be passed to callback
 *                   function later on
 * @param pas_user_cmd Pointer to structure tied with processed command
 * @return True if parameters are valid, false otherwise
 */
static bool _are_arguments_valid(ts_usc_cb_args *ps_cb_args,
                                 const ts_usc_cmd_cfg *pas_user_cmd);

/**
 * @brief It prints new line and then defined number of spaces
 * @param u16_num_of_spaces Number of spaces
 */
static void _print_new_line_and_spaces(const uint16_t u16_num_of_spaces);

/**
 * @brief Check if core is configured or not
 * @return True if correctly configured, false otherwise
 */
static bool _is_core_configured(void);
// =========================| High level functions |==========================
te_usc_error_code usc_configure(ts_usc_config_args *ps_args) {
  if (!ps_args) {
    return USC_ERROR_EMPTY_POINTER;
  }

  // Before making full copy, check arguments

  /* Following functions does not have to be defined:
   *   * Get password (then assume no password)
   *   * Enter service mode
   *   * Leave service mode
   */
  if ((!ps_args->pf_read_itf_cb) || (!ps_args->pf_write_itf_cb) ||
      (!ps_args->pf_get_list_of_cmds_cb) || (!ps_args->pf_get_num_of_cmds_cb) ||
      (!ps_args->pf_delay_ms_cb)) {
    return USC_ERROR_EMPTY_POINTER;
  }

  // Everything seems to be valid, copy settings
  memcpy(&ms_runtime.s_cfg, ps_args, sizeof(ms_runtime.s_cfg));

  // The configuration can be changed "on the fly", so no need to reset state
  // machine or reset user input buffer

  return USC_OK;
}

te_usc_error_code usc_task_exec(void) {
  // If core is not configured, exit task gracefully
  if (!_is_core_configured()) {
    return USC_ERROR_NOT_CONFIGURED;
  }

  switch (ms_runtime.e_state_actual) {
    case UI_SRVCE_WAIT_FOR_LOGIN:
      _wait_for_login(&ms_runtime.e_state_previous, &ms_runtime.e_state_actual,
                      ms_runtime.ac_user_input_buffer,
                      &ms_runtime.u16_user_input_buffer_write_idx,
                      &ms_runtime.s_cfg);
      break;
    case UI_SRVCE_WAIT_FOR_CMD:
      _wait_for_cmd(&ms_runtime.e_state_previous, &ms_runtime.e_state_actual,
                    ms_runtime.ac_user_input_buffer,
                    &ms_runtime.u16_user_input_buffer_write_idx,
                    &ms_runtime.s_cfg);
      break;
    case UI_SRVCE_PROCESS_CMD:
      _process_cmd(&ms_runtime.e_state_previous, &ms_runtime.e_state_actual,
                   ms_runtime.ac_user_input_buffer,
                   &ms_runtime.u16_user_input_buffer_write_idx,
                   &ms_runtime.s_cfg);
      break;
    default:
      // This should not happen. But if so, let's reset this machinery
      usc_task_reset();
  }

  // Not much to decide. Simply OK
  return USC_OK;
}

void usc_task_reset(void) {
  // Reset state
  ms_runtime.e_state_actual = UI_SRVCE_WAIT_FOR_LOGIN;
  ms_runtime.e_state_previous = UI_SRVCE_PROCESS_CMD;

  // Clean up user input
  memset(ms_runtime.ac_user_input_buffer, 0x00,
         sizeof(ms_runtime.ac_user_input_buffer));
  ms_runtime.u16_user_input_buffer_write_idx = 0;
}
// ========================| Middle level functions |=========================

te_usc_special_key usc_wait_for_enter_esc(char *pac_user_input_buffer) {
  // Write index for buffer - start from 0
  uint16_t u16_write_idx = 0;
  te_usc_special_key e_spec_key = KEY_NONE;

  while ((e_spec_key != KEY_ENTER) && (e_spec_key != KEY_ESC)) {
    if (e_spec_key == KEY_ESC) {
      break;
    }
    // Reset write index
    u16_write_idx = 0;

    // Release task for a while
    ms_runtime.s_cfg.pf_delay_ms_cb(UI_SERVICE_TASK_RECOMMENDED_PERIOD_MS);

    // Do not force reset (0). Let function work "standard way"
    e_spec_key = _wait_for_user(pac_user_input_buffer, &u16_write_idx,
                                pac_press_enter_when_ready, 0);
  }

  return e_spec_key;
}

te_usc_special_key usc_check_for_enter_esc(char *pac_user_input_buffer) {
  // Write index for buffer - start from 0
  uint16_t u16_write_idx = 0;

  // No initial string shall be printed (0). Force reset internal logic (1)
  return _wait_for_user(pac_user_input_buffer, &u16_write_idx, 0, 1);
}
// ==========================| Low level functions |==========================
void usc_print_str(const char *pac_string) {
  // Print text with maximum possible delay
  ms_runtime.s_cfg.pf_write_itf_cb((uint8_t *)pac_string, strlen(pac_string),
                                   _MAX_DELAY_MS);
}

void usc_printf(const char *fmt, ...) {
  // The sprintf will push final string here
  char ac_buffer[UI_SERVICE_PRINTF_BUFFER];

  // Whoosh.... black magic
  va_list va;
  va_start(va, fmt);
  vsprintf(ac_buffer, fmt, va);
  va_end(va);

  // And print it
  usc_print_str(ac_buffer);
}

void usc_print_ok_fail(te_usc_error_code e_err_code) {
  if (e_err_code) {
    usc_printf("[FAIL] %s", usc_error_code_to_string(e_err_code));
  } else {
    usc_print_str(pac_ok);
  }
}

const char *usc_error_code_to_string(te_usc_error_code e_error_code) {
  switch (e_error_code) {
    case USC_OK:
      return "OK";
    case USC_ERROR:
      return "error";
    case USC_ERROR_TIMEOUT:
      return "timeout";
    case USC_ERROR_EMPTY_POINTER:
      return "empty pointer";
    case USC_ERROR_INVALID_PARAMETER:
      return "invalid parameter";
    case USC_ERROR_NOT_CONFIGURED:
      return "not configured";
    default:
      // Should not happen, but if so, print at least something
      return "???";
  }
}
// ===========================| Predefined texts |============================
inline const char *usc_txt_ok(void) { return pac_ok; }

inline const char *usc_txt_fail(void) { return pac_fail; }

inline const char *usc_txt_ivalid_arg(void) { return pac_invalid_argument; }

inline const char *usc_txt_can_not_convert_to_int(void) {
  return pac_can_not_convert;
}

inline const char *usc_txt_out_of_range(void) { return pac_out_of_range; }
// ==========================| Internal functions |===========================
// ====================| Internal functions: high level |=====================
static void _wait_for_login(te_usc_state *pe_state_prev,
                            te_usc_state *pe_state_actual,
                            char *pac_user_input_buffer,
                            uint16_t *pu16_user_input_buffer_write_idx,
                            const ts_usc_config_args *ps_cfg) {
  // Pointers should not be empty
  assert(pe_state_prev);
  assert(pe_state_actual);
  assert(pac_user_input_buffer);

  assert(*pe_state_actual == UI_SRVCE_WAIT_FOR_LOGIN);

  if (!ps_cfg->pf_get_password_cb) {
    // Password is not required -> speed it up
    *pe_state_prev = *pe_state_actual;
    *pe_state_actual = UI_SRVCE_WAIT_FOR_CMD;
    return;
  }

  te_usc_special_key e_key;

  if (*pe_state_prev != *pe_state_actual) {
    // Show login
    usc_print_str(pac_login);
  }
  *pe_state_prev = *pe_state_actual;

  // Do not force reset (0). Let function work "standard way"
  e_key = _wait_for_user(pac_user_input_buffer,
                         pu16_user_input_buffer_write_idx, "\npassword: ", 0);
  if (e_key == KEY_ENTER) {
    // Load password. If not needed, then just load empty string
    const char *pac_pwd = ps_cfg->pf_get_password_cb();

    assert(ps_cfg->pf_get_password_cb);

    // Password is loaded, compare it
    if (_CMD_NO_PARAM(pac_pwd)) {
      // Login successful - ready to receive commands
      usc_print_str(pac_ok);

      // Send request to UI to switch to "Service mode" too (disable
      // all tasks) if callback is set
      if (ps_cfg->pf_enter_service_mode_cb) {
        ps_cfg->pf_enter_service_mode_cb();
      }

      // If approval from upper layer is needed, wait a bit
      if (ps_cfg->pf_enter_service_mode_is_ready_cb) {
        // Wait until processing is ready
        while (!ps_cfg->pf_enter_service_mode_is_ready_cb()) {
          ps_cfg->pf_delay_ms_cb(_LOADING_PERIOD_MS);
          usc_print_str(".");
        }
      }

      usc_print_str("\n");

      *pe_state_actual = UI_SRVCE_WAIT_FOR_CMD;
    } else {
      // Wrong password
      usc_print_str(pac_invalid_password);
      usc_print_str(pac_login);
    }
  } else if (e_key == KEY_ESC) {
    // Show login again
    usc_print_str(pac_login);
  } else {
    // do nothing, keep collecting data
  }
}

static void _wait_for_cmd(te_usc_state *pe_state_previous,
                          te_usc_state *pe_state_actual,
                          char *pac_user_input_buffer,
                          uint16_t *pu16_user_input_buffer_write_idx,
                          const ts_usc_config_args *ps_cfg) {
  // Pointers should not be empty
  assert(pe_state_previous);
  assert(pe_state_actual);
  assert(pac_user_input_buffer);
  assert(pu16_user_input_buffer_write_idx);

  assert(*pe_state_actual == UI_SRVCE_WAIT_FOR_CMD);

  te_usc_special_key e_key;

  if ((*pe_state_previous != *pe_state_actual) &&
      (*pe_state_previous == UI_SRVCE_WAIT_FOR_LOGIN)) {
    _print_help();
  }
  *pe_state_previous = *pe_state_actual;

  // Do not force reset (0). Let function work "standard way"
  e_key = _wait_for_user(pac_user_input_buffer,
                         pu16_user_input_buffer_write_idx, "\n$ ", 0);
  if (e_key == KEY_ENTER) {
    // Process data
    *pe_state_actual = UI_SRVCE_PROCESS_CMD;
  } else {
    // do nothing. Function "_waitForUser" will print "$" sign again
  }
}

static void _process_cmd(te_usc_state *pe_state_previous,
                         te_usc_state *pe_state_actual,
                         char *pac_user_input_buffer,
                         uint16_t *pu16_user_input_buffer_write_idx,
                         const ts_usc_config_args *ps_cfg) {
  // Pointers should not be empty
  assert(pe_state_previous);
  assert(pe_state_actual);
  assert(pac_user_input_buffer);
  assert(pu16_user_input_buffer_write_idx);

  assert(*pe_state_actual == UI_SRVCE_PROCESS_CMD);

  *pe_state_previous = *pe_state_actual;

  // Wait for another command - default behavior
  *pe_state_actual = UI_SRVCE_WAIT_FOR_CMD;

  // Exit command is available only if password is required -> check if
  // "get password" callback is set
  if (_CMD_NO_PARAM(CMD_EXIT) && ps_cfg->pf_get_password_cb) {
    usc_print_str(pac_log_off);

    // Send request to UI that now it is safe to go back to normal mode
    if (ps_cfg->pf_leave_service_mode_cb) {
      ps_cfg->pf_leave_service_mode_cb();
    }

    // Wait to confirmation that leaving service mode is now safe (if callback
    // is set)
    if (ps_cfg->pf_leave_service_mode_is_ready_cb) {
      while (!ps_cfg->pf_leave_service_mode_is_ready_cb()) {
        ps_cfg->pf_delay_ms_cb(_LOADING_PERIOD_MS);
        usc_print_str(".");
      }
    }

    *pe_state_actual = UI_SRVCE_WAIT_FOR_LOGIN;
  } else {
    // Search in user commands
    if (_was_user_cmd_recognized_and_processed(
            pac_user_input_buffer, pu16_user_input_buffer_write_idx)) {
      // Command was found and processed
    } else {
      // Unknown command. Let user know by printing help again. Maybe just typo
      usc_print_str(pac_unknown_cmd);
      _print_help();
    }
  }
}

static te_usc_error_code _print_help(void) {
  usc_print_str(pac_help_header);

  // Try to print generated help from user module
  const ts_usc_cmd_cfg *pas_user_cmds =
      ms_runtime.s_cfg.pf_get_list_of_cmds_cb();
  const uint32_t u32_num_of_cmds = ms_runtime.s_cfg.pf_get_num_of_cmds_cb();

  /* Counts "printed" number of characters on current line. It helps make
   * help output nice
   */
  uint16_t u16_line_character_cnt;

  // Over 2 billion of commands? Really?
  assert(u32_num_of_cmds < 0xFFFFFFFF);

  // Process command after command
  for (uint32_t u32_current_cmd_idx = 0; u32_current_cmd_idx < u32_num_of_cmds;
       u32_current_cmd_idx++) {
    // Start with printing command name. Before every command is small space
    usc_print_str(ms_help_txt.pac_help_space);

    // Set characters per line counter, since this is new fresh command
    u16_line_character_cnt = strlen(ms_help_txt.pac_help_space);

    // String should not be empty obviously
    assert(pas_user_cmds[u32_current_cmd_idx].pac_command);

    usc_print_str(pas_user_cmds[u32_current_cmd_idx].pac_command);
    u16_line_character_cnt +=
        strlen(pas_user_cmds[u32_current_cmd_idx].pac_command);

    // If argument required, add it to printed string
    if (pas_user_cmds[u32_current_cmd_idx].b_require_argument) {
      // In any case add space to separate command and argument
      usc_print_str(" ");
      u16_line_character_cnt += 1;

      // If argument example is defined, print it
      if (pas_user_cmds[u32_current_cmd_idx].s_arg_cfg.pac_argument_example !=
          0) {
        usc_print_str(
            pas_user_cmds[u32_current_cmd_idx].s_arg_cfg.pac_argument_example);

        u16_line_character_cnt += strlen(
            pas_user_cmds[u32_current_cmd_idx].s_arg_cfg.pac_argument_example);
      } else {
        // Optional argument is not defined
        usc_print_str(ms_help_txt.pac_default_arg);
        u16_line_character_cnt += strlen(ms_help_txt.pac_default_arg);
      }
    }

    // Separate command and help
    usc_print_str(ms_help_txt.pac_cmd_and_help_separator);
    u16_line_character_cnt += strlen(ms_help_txt.pac_cmd_and_help_separator);

    // Print help and wrap lines if required
    _print_help_wrap_lines(pas_user_cmds[u32_current_cmd_idx].pac_help,
                           u16_line_character_cnt);

    // End of the command - final new line
    usc_print_str("\n");
  }

  // Print help tail - if password is required
  if (ms_runtime.s_cfg.pf_get_password_cb) {
    usc_print_str(pac_help_if_logged);
  }

  // Do not bother about error code. If not printed, there is nothing to do
  // anyway
  return USC_OK;
}
// ===================| Internal functions: middle level |====================
static bool _was_user_cmd_recognized_and_processed(
    char *pac_user_input_buffer, uint16_t *pu16_user_input_buffer_write_idx) {
  // Keep status if current command match or not
  bool b_command_match;

  // Load command list
  const ts_usc_cmd_cfg *pas_user_cmds =
      ms_runtime.s_cfg.pf_get_list_of_cmds_cb();
  const uint32_t u32_num_of_cmds = ms_runtime.s_cfg.pf_get_num_of_cmds_cb();

  // Over 2 billion of commands? Really?
  assert(u32_num_of_cmds < 0xFFFFFFFF);

  // Arguments for callback function will be pushed here so then we can
  // easily pass to called command.
  // Input buffer will not change - can assign once
  ts_usc_cb_args s_cb_args = {.pac_input_string = pac_user_input_buffer};

  // Plan is to go through every single user command and compare it with
  // input. If command is found, call callback function and pass given input
  // as argument
  for (uint32_t u32_current_cmd_idx = 0; u32_current_cmd_idx < u32_num_of_cmds;
       u32_current_cmd_idx++) {
    // Reset this variable. Here we can not know if match or not
    b_command_match = false;

    // There is slightly different processing for commands which accept and
    // which does not accept argument
    if (pas_user_cmds[u32_current_cmd_idx].b_require_argument) {
      if (_CMD_WITH_PARAM(pas_user_cmds[u32_current_cmd_idx].pac_command)) {
        b_command_match = true;

        // Argument required - set pointer where argument should be. Note that
        // we need to move after command -> +1
        s_cb_args.pac_args =
            pac_user_input_buffer +
            strlen(pas_user_cmds[u32_current_cmd_idx].pac_command) + 1;

        if (_are_arguments_valid(&s_cb_args,
                                 &pas_user_cmds[u32_current_cmd_idx])) {
          // Arguments are valid, everything is cool
        } else {
          // Invalid arguments. Return now
          usc_print_str(pac_invalid_argument);

          return true;
        }
      }

    } else {
      // Argument is not required
      if (_CMD_NO_PARAM(pas_user_cmds[u32_current_cmd_idx].pac_command)) {
        b_command_match = true;

        // Argument not required - clean up structure
        s_cb_args.pac_args = 0;
        s_cb_args.u_value.u32_value = 0;
      }
    }

    if (b_command_match) {
      // Execute callback. It should not be empty obviously
      assert(pas_user_cmds[u32_current_cmd_idx].pf_cmd_callback);

      pas_user_cmds[u32_current_cmd_idx].pf_cmd_callback(&s_cb_args);

      // Jump out of this function. Command recognized
      return true;
    }
  }  // /for all commands

  // If we get here, no command actually matched
  return false;
}

static te_usc_special_key _wait_for_user(
    char *pac_user_input_buffer, uint16_t *pu16_user_input_buffer_write_idx,
    const char *pac_initial_string, const bool b_force_reset) {
  assert(pac_user_input_buffer);
  assert(pu16_user_input_buffer_write_idx);

  // Reset logic?
  static bool b_reset = true;

  // Tell where special key was found
  uint16_t u16_key_idx = -1;

  // If previous state was processing command, clear buffers. Also if upper
  // layer reset write index, it signalize that logic should be reset too
  if (b_reset | b_force_reset) {
    *pu16_user_input_buffer_write_idx = 0;

    // Print initial string only if defined
    if (pac_initial_string) {
      usc_print_str(pac_initial_string);
    }

    b_reset = false;
  }

  // Temporary buffer
  char ac_rx_bytes[UI_SERVICE_RX_BUFFER_SIZE] = {0};

  uint16_t u16_rx_bytes_cnt;
  ms_runtime.s_cfg.pf_read_itf_cb((uint8_t *)ac_rx_bytes, sizeof(ac_rx_bytes),
                                  UI_SERVICE_RX_USER_INPUT_TIMEOUT_MS,
                                  &u16_rx_bytes_cnt);

  // If nothing to process, exit preliminary
  if (u16_rx_bytes_cnt == 0) {
    return KEY_NONE;
  }
  ///@todo Solve "problem" with deleting "$ " when pushing backspace

  // Put data to user input buffer
  assert(u16_rx_bytes_cnt > 0);

  te_usc_error_code e_err_code = _append_buffer(
      ac_rx_bytes, (uint16_t)u16_rx_bytes_cnt, pac_user_input_buffer,
      pu16_user_input_buffer_write_idx, UI_SERVICE_RX_BUFFER_SIZE);

  // Some terminal specific thing. Do not re-print ESC character (0x1B), but
  // print something else
  for (int i_byte_idx = 0; i_byte_idx < u16_rx_bytes_cnt; i_byte_idx++) {
    // If ESC character found, replace it
    if (ac_rx_bytes[i_byte_idx] == 0x1B) {
      ac_rx_bytes[i_byte_idx] = '^';
    }
  }

  // Print input back to user to have some feedback. Maximum timeout is fine
  ms_runtime.s_cfg.pf_write_itf_cb((uint8_t *)ac_rx_bytes, u16_rx_bytes_cnt,
                                   _MAX_DELAY_MS);

  if (e_err_code) {
    // Something went wrong -> re-initialize
    b_reset = true;
    return KEY_NONE;
  }

  // Search for keys
  assert(KEY_NONE == 0);
  for (te_usc_special_key e_key = (te_usc_special_key)(KEY_NONE + 1);
       e_key < KEY_MAX; e_key++) {
    if (_find_special_key(e_key, pac_user_input_buffer,
                          *pu16_user_input_buffer_write_idx, &u16_key_idx)) {
      // Write new line, so new written text will not be overwritten
      usc_print_str("\n");

      // Write null to position of found character, so input will be
      // treated as string
      pac_user_input_buffer[u16_key_idx] = 0x00;

      // Reset statemachine of this function
      b_reset = true;
      return e_key;
    }
  }

  // Return whatever was set - should be KEY_NONE
  return KEY_NONE;
}

static void _print_help_wrap_lines(char *pac_help,
                                   uint16_t u16_position_of_help_content) {
  // Number of character available on line when considering position of help
  // content (additional spaces)
  uint16_t u16_available_characters_for_line =
      UI_SERVICE_HELP_MAX_CHAR_PER_LINE - u16_position_of_help_content;

  // Check if string simply fits on the remaining space in current line. If
  // yes, then it is super easy - just print it.
  if ((u16_position_of_help_content < UI_SERVICE_HELP_MAX_CHAR_PER_LINE) &&
      (strlen(pac_help) <= u16_available_characters_for_line)) {
    // Simply print help with maximum delay
    ms_runtime.s_cfg.pf_write_itf_cb((uint8_t *)pac_help, strlen(pac_help),
                                     _MAX_DELAY_MS);

    return;
  }

  // If position of help content is already over required limit, let's try to
  // use whole line width
  if (u16_position_of_help_content >= UI_SERVICE_HELP_MAX_CHAR_PER_LINE) {
    u16_position_of_help_content = 0;

    u16_available_characters_for_line = UI_SERVICE_HELP_MAX_CHAR_PER_LINE;
  }

  // Words are separated by spaces. When space is found, it is registered it's
  // position on the row, so if we're "overshoot", we can "go back" and break
  // the line before reaching limit (number of characters per line)
  uint16_t u16_space_idx;

  do {
    // Reset index for localization space
    u16_space_idx = 0;

    // Search for space or terminator (null character)
    for (uint16_t u16_char_idx = 0; u16_char_idx <= strlen(pac_help);
         u16_char_idx++) {
      // If space or terminator is here, check if still fits on current line or
      // not
      if ((pac_help[u16_char_idx] == ' ') || (pac_help[u16_char_idx] == 0)) {
        if (u16_char_idx >= u16_available_characters_for_line) {
          // Overshoot -> need to print everything up to previous space,
          // unless this is very first space found
          if (u16_space_idx == 0) {
            // Print up to this character, since we would not fit into limit
            // anyway (no way to split)
            ms_runtime.s_cfg.pf_write_itf_cb((uint8_t *)pac_help, u16_char_idx,
                                             _MAX_DELAY_MS);

            if (pac_help[u16_char_idx] == 0) {
              // Found null -> no need to print more
              return;
            }
            // Move pointer to next character
            pac_help += u16_char_idx + 1;

          } else {
            // Space was found earlier, so let's use it as separator
            ms_runtime.s_cfg.pf_write_itf_cb((uint8_t *)pac_help, u16_space_idx,
                                             _MAX_DELAY_MS);

            if (pac_help[u16_char_idx] == 0) {
              // Found null -> no need to print more
              return;
            }

            // Move pointer right behind previously found space
            pac_help += u16_space_idx + 1;
          }

          // Add new line and dummy spaces
          _print_new_line_and_spaces(u16_position_of_help_content);

          // Break current loop to start from "next character"
          break;
        } else {
          // Text does not overshoot limit
          if (pac_help[u16_char_idx] == 0) {
            // Found null -> just print what is in the buffer
            ms_runtime.s_cfg.pf_write_itf_cb((uint8_t *)pac_help,
                                             strlen(pac_help), _MAX_DELAY_MS);
            return;
          }

          // Still fit -> store space index
          u16_space_idx = u16_char_idx;
        }
      }
    }

  } while (strlen(pac_help));
}
// =====================| Internal functions: low level |=====================

static te_usc_error_code _append_buffer(const char *pac_src,
                                        const uint16_t u16_src_size,
                                        char *pac_dst,
                                        uint16_t *pu16_dst_write_idx,
                                        const uint16_t u16_dst_buffer_size) {
  // If nothing to process
  if (u16_src_size == 0) {
    return USC_OK;
  }

  for (uint16_t u16_proc_byte_cnt = 0; u16_proc_byte_cnt < u16_src_size;
       u16_proc_byte_cnt++) {
    /* Check if there is "backspace" character. If yes, do not move forward
     * but decrease write pointer, so previous character will be deleted
     */
    if (pac_src[u16_proc_byte_cnt] == '\b') {
      // It is pointless to write "backspace" character - skip it
      if (*pu16_dst_write_idx >= 1) {
        (*pu16_dst_write_idx)--;
        /* Print to console space (overwrite old character) and print
         * backspace
         */
        usc_print_str(" \b");
      } else {
        // Do not decrease Write index - keep it as it is
      }

    }  ///@todo Handle other non-character symbols
    else {
      // Copy character, increase write index
      pac_dst[*pu16_dst_write_idx] = pac_src[u16_proc_byte_cnt];

      // Increase write index & check value
      (*pu16_dst_write_idx)++;
      if (*pu16_dst_write_idx >= u16_dst_buffer_size) {
        return USC_ERROR_NO_MEMORY;
      }
    }
  }
  return USC_OK;
}

static bool _find_special_key(const te_usc_special_key e_key,
                              const char *pac_buff, const uint16_t u16_max_idx,
                              uint16_t *pu16_key_idx) {
  for (uint16_t u16_idx = 0; u16_idx < u16_max_idx; u16_idx++) {
    // According to the key, process
    switch (e_key) {
      case KEY_NONE:
        // This should not happen. This is invalid parameter
        assert(0);
        return KEY_NONE;

      case KEY_ENTER:
        if ((pac_buff[u16_idx] == '\n') || (pac_buff[u16_idx] == '\r')) {
          *pu16_key_idx = u16_idx;
          return true;
        }
        break;
      case KEY_ESC:
        // ASCI ESC - 0x1B
        if (pac_buff[u16_idx] == 0x1B) {
          *pu16_key_idx = u16_idx;
          return true;
        }
        break;
      case KEY_MAX:
        // This should not happen. This is invalid parameter
        assert(0);
        return KEY_NONE;
    }
  }
  return false;
}

static bool _are_arguments_valid(ts_usc_cb_args *ps_cb_args,
                                 const ts_usc_cmd_cfg *pas_user_cmd) {
  assert(ps_cb_args);
  assert(pas_user_cmd);
  assert(pas_user_cmd->b_require_argument);

  bool b_arg_valid = false;

  // Define error code variable and find where argument is stored.
  // Typically arguments are after command
  te_usc_error_code e_err_code = USC_ERROR;

  // Argument required - check it if possible
  if (pas_user_cmd->s_arg_cfg.e_arg_type == UI_SRVCE_CMD_ARG_TYPE_STR) {
    // Do not check argument. Assume that it is valid
    b_arg_valid = true;

  } else if ((pas_user_cmd->s_arg_cfg.e_arg_type ==
              UI_SRVCE_CMD_ARG_TYPE_INT) ||
             (pas_user_cmd->s_arg_cfg.e_arg_type ==
              UI_SRVCE_CMD_ARG_TYPE_HEX)) {
    // Call appropriate conversion function
    if (pas_user_cmd->s_arg_cfg.e_arg_type == UI_SRVCE_CMD_ARG_TYPE_INT) {
      e_err_code = usu_str_dec_to_int_32(ps_cb_args->pac_args,
                                         &ps_cb_args->u_value.i32_value);
    } else {
      e_err_code = usu_str_hex_to_uint_32(ps_cb_args->pac_args,
                                          &ps_cb_args->u_value.u32_value);
    }

    // Check conversion and requirements
    if (e_err_code) {
      // Can not convert to integer
      b_arg_valid = false;
      usc_print_str(pac_can_not_convert);

    } else if ((pas_user_cmd->s_arg_cfg.i32_min == 0) &&
               (pas_user_cmd->s_arg_cfg.i32_max == 0)) {
      // Do not check value
      b_arg_valid = true;

    } else if ((ps_cb_args->u_value.i32_value <
                pas_user_cmd->s_arg_cfg.i32_min) ||
               (ps_cb_args->u_value.i32_value >
                pas_user_cmd->s_arg_cfg.i32_max)) {
      // If out of range
      b_arg_valid = false;
      usc_print_str(pac_out_of_range);

    } else {
      // No problem then
      b_arg_valid = true;
    }  // /check conversion results
  } else {
    // Should not happen. Unexpected case for eArgType
    assert(0);
  }

  return b_arg_valid;
}

static void _print_new_line_and_spaces(const uint16_t u16_num_of_spaces) {
  const char ac_newline[] = "\n";
  const char ac_space[] = " ";

  ms_runtime.s_cfg.pf_write_itf_cb((uint8_t *)ac_newline, sizeof(ac_newline),
                                   _MAX_DELAY_MS);

  for (uint16_t u16spaceIdx = 0; u16spaceIdx < u16_num_of_spaces;
       u16spaceIdx++) {
    ms_runtime.s_cfg.pf_write_itf_cb((uint8_t *)ac_space, sizeof(ac_space),
                                     _MAX_DELAY_MS);
  }
}

static bool _is_core_configured(void) {
  // Let's use small trick. It would be possible to have one extra variable,
  // which would keep information if configuration was successfully set or not,
  // but instead, let's check if required pointers are set or not. This is more
  // robust way how to do it
  const ts_usc_config_args *ps_args = &ms_runtime.s_cfg;
  if ((!ps_args->pf_read_itf_cb) || (!ps_args->pf_write_itf_cb) ||
      (!ps_args->pf_get_list_of_cmds_cb) || (!ps_args->pf_get_num_of_cmds_cb) ||
      (!ps_args->pf_delay_ms_cb)) {
    // Not all required functions are set -> not configured
    return false;
  } else {
    return true;
  }
}
