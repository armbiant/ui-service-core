/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration example file for Service UI core
 *
 * This file basically show which additional options can be defined in
 * application specific ui_service_cfg.h file.
 */
#ifndef __UI_SERVICE_CORE_CFG_H__
#define __UI_SERVICE_CORE_CFG_H__
// ===============================| Includes |================================
#include "ui_service_cfg.h"  // User functions and callback mapping mainly

// Following allow to load "global" settings from one file
// When compiler support it, check if user file exists and if yes, include it
#if defined __has_include
#if __has_include("cfg.h")
#include "cfg.h"
#endif
#endif
// ================================| Defines |================================

// ============================| Default values |=============================

#ifndef UI_SERVICE_RX_BUFFER_SIZE
/**
 * @brief Buffer size in Bytes
 *
 * Usually commands should be really short, so something like 256 should be
 * more than enough.
 */
#define UI_SERVICE_RX_BUFFER_SIZE (256)
#endif  // UI_SERVICE_RX_BUFFER_SIZE

#ifndef UI_SERVICE_RX_USER_INPUT_TIMEOUT_MS
/**
 * @brief Define period in ms within UART RX function stop scanning
 *
 * For best user experience (fast reply from MCU) keep this value low
 */
#define UI_SERVICE_RX_USER_INPUT_TIMEOUT_MS (5)
#endif  // UI_SERVICE_RX_USER_INPUT_TIMEOUT_MS

#ifndef UI_SERVICE_HELP_MAX_CHAR_PER_LINE
/**
 * @brief Maximum number of characters on line when printing help
 *
 * Help algorithm try to split words and keep this limit in order to make
 * output beautiful.
 */
#define UI_SERVICE_HELP_MAX_CHAR_PER_LINE (80)
#endif  // UI_SERVICE_HELP_MAX_CHAR_PER_LINE

#ifndef UI_SERVICE_PRINTF_BUFFER
/**
 * @brief When using usc_printf there have to be some buffer
 *
 * This buffer is required to "pack" string somewhere. Once function is
 * returned, RAM is released, but for that moment might be needed. Typically
 * it is not necessary to print more than 1~2 lines, but just in case it is
 * better to make this array bigger.
 */
#define UI_SERVICE_PRINTF_BUFFER (4 * UI_SERVICE_HELP_MAX_CHAR_PER_LINE)
#endif  // UI_SERVICE_PRINTF_BUFFER

#endif  // __UI_SERVICE_CORE_CFG_H__
